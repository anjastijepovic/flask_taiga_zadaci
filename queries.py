from db_connection import cursor, db
from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort
from datetime import datetime
from mysql.connector import Error



#EMPLOYEES

def get_employees():
    sql = ("SELECT * FROM employees")
    cursor.execute(sql)
    result = cursor.fetchall()
    return result
    

def get_employee_from_num(employee_number):
    sql = (f"SELECT * FROM employees WHERE employeeNumber = '{employee_number}'")
    cursor.execute(sql)
    row = cursor.fetchone()
    return row


def add_employee(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle):
    sql = ("INSERT INTO employees(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")
    try:
        cursor.execute(sql, (employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle,))
        db.commit()
        return True
    except Error as error:
        return error.msg, 409
   

def delete_employee(employeeNumber):
    sql = ("DELETE FROM employees WHERE employeeNumber = %s")
    try:
        cursor.execute(sql, (employeeNumber,))
        db.commit()
        return "Employee is deleted." 
    except Error as error:
        return error.msg, 404

# dodaj i za update try except itd
    
def update_employee(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle):
    sql = ("UPDATE employees SET lastName = %s, firstName = %s, extension = %s, email = %s, officeCode = %s, reportsTo = %s, jobTitle = %s WHERE employeeNumber = %s")
    try:
        cursor.execute(sql, (lastName, firstName, extension, email, officeCode, reportsTo, jobTitle, employeeNumber,))
        db.commit()
        return "Successfully updated an employee." 
    except Error as error:
        return error.msg, 404
    
    


#CUSTOMERS
    
def get_customers_with_employee_num(sales_emp_num):
    sql = (f"SELECT customerNumber, customerName, contactLastName, contactFirstName, phone, addressLine1, addressLine2, city, state, postalCode, country, salesRepEmployeeNumber FROM customers WHERE salesRepEmployeeNumber = '{sales_emp_num}'")
    cursor.execute(sql)
    result = cursor.fetchall() #lista
    return result

#creditLimit je decimal, dodati kasnije

def get_customer_from_number(customer_number):
    sql = (f"SELECT * FROM customers WHERE customerNumber = '{customer_number}'")
    cursor.execute(sql)
    row = cursor.fetchone()
    return row



#PAYMENTS

def payment_amount(customer_number, payment_date):
    sql = (f"SELECT SUM(amount) FROM payments WHERE customerNumber = '{customer_number}' and paymentDate >= '{payment_date}'")
    cursor.execute(sql)
    row = cursor.fetchone()
    total_amount_paid = row[0] if row[0] is not None else 0
    total_amount_paid_float = float(total_amount_paid)
    cursor.fetchall() 
    return total_amount_paid_float


def get_payment_from_customer_num(customer_number):
    sql = (f"SELECT * FROM payments WHERE customerNumber = '{customer_number}'")
    cursor.execute(sql)
    row = cursor.fetchone() #tuple
    cursor.fetchall() 
    return row

#print(payment_amount(484, datetime(2003, 11, 29)))
#print(get_payment_from_customer_num(48477))
# print(get_payment_from_customer_num(484))



#PRODUCTS

# def get_products():
#     sql = ("SELECT * FROM products")
#     cursor.execute(sql)
#     result = cursor.fetchall()
#     return result


# # da promijenimo decimalne u float:

def get_products():
    cursor.execute('SELECT * FROM products;')
    sql = cursor.fetchall()
    result = []
    for product in sql:
        product_obj = {
            "productCode": product[0],
            "productName": product[1],
            "productLine": product[2],
            "productScale": product[3],
            "productVendor": product[4],
            "productDescription": product[5],
            "quantityInStock": product[6],
            "buyPrice": float(product[7]),
            "MSRP": float(product[8])
            }
        result.append(product_obj)
    return result

# drugi nacin
# python3 -m pip install simplejson
# python3 -m  pip freeze
# return jsonify(...)

def get_all():
    sql = ('SELECT * FROM classicmodels.products')
    cursor.execute(sql)
    result = cursor.fetchall()
    return result


# print(get_products())


def get_product_from_code(product_code):
    sql = (f"SELECT * FROM products WHERE productCode = '{product_code}'")
    cursor.execute(sql)
    row = cursor.fetchone()
    return row


def add_product(productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP):
    sql = ("INSERT INTO products(productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
    try:
        cursor.execute(sql, (productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP))
        db.commit()
        return "Successfully added a product." 
    except Error as error:
        return error.msg, 404


def delete_product(product_code):
    sql = ("DELETE FROM products WHERE productCode = %s")
    try:
        cursor.execute(sql, (product_code,))
        db.commit()
        return 'Product successfully deleted.'
    except Error as error:
        return error.msg, 404


def update_product(productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP):
    sql = ("UPDATE products SET productName = %s, productLine = %s, productScale = %s, productVendor = %s, productDescription = %s, quantityInStock = %s, buyPrice = %s,  MSRP = %s WHERE productCode = %s")
    try:
        cursor.execute(sql, (productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP, productCode,))
        db.commit()
        return "Successfully updated a product." 
    except Error as error:
        return error.msg, 404


# ORDERS. task 10
    
'''
Provjeriti da li postoji customer
Provjeriti da li ima dovoljno products in stock
Dodati objekat u orders i orderdetails
'''

def get_order_from_number(order_number):
    sql = (f"SELECT orderNumber FROM orders WHERE orderNumber = '{order_number}'")
    cursor.execute(sql)
    row = cursor.fetchone()
    return row

# def customer_exists(customer_number):
#     customer = get_customer_from_number(customer_number)
#     if customer is None:
#         return False
#     return True


def product_has_sufficient_stock(productCode, quantityOrdered):
    sql = "SELECT quantityInStock FROM products WHERE productCode = %s"
    cursor.execute(sql, (productCode,))
    quantity_in_stock = cursor.fetchone()[0]
    return quantity_in_stock >= quantityOrdered


def add_order_and_details(orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber, productCode, quantityOrdered, priceEach, orderLineNumber):

    sql_order = "INSERT INTO orders(orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    cursor.execute(sql_order, (orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber))
    
    sql_order_detail = "INSERT INTO orderdetails(orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber) VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(sql_order_detail, (orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber))
    
    db.commit()

# task 11
# customer number from orders
    
def get_customer_number(order_number):
    sql = "SELECT customerNumber FROM orders WHERE orderNumber = %s"
    cursor.execute(sql, (order_number,))
    result = cursor.fetchone()
    return result[0]  # Return customerNumber


# quantityOrdered and priceEach from orderdetails 
    
def get_order_details(order_number):
    sql = "SELECT quantityOrdered, priceEach FROM orderdetails WHERE orderNumber = %s"
    cursor.execute(sql, (order_number,))
    result = cursor.fetchall()
    return result #list of tuples [(quantityOrdered, priceEach), ...]


def get_payment_from_order_number(order_number):
    customer_number = get_customer_number(order_number)
    sql = (f"SELECT * FROM payments WHERE customerNumber = '{customer_number}'")
    cursor.execute(sql)
    row = cursor.fetchone()
    return row


def add_payment(order_number, check_number):
    customer_number = get_customer_number(order_number)
    payment_date = datetime.now().date().isoformat()
    order_details = get_order_details(order_number)
    amount = sum(quantity * price for quantity, price in order_details)

    sql_order = "INSERT INTO payments(customerNumber, checkNumber, paymentDate, amount) VALUES (%s, %s, %s, %s)"
    try:
        cursor.execute(sql_order, (customer_number, check_number, payment_date, amount))
        db.commit()
        return "Successfully added a payment." 
    except Error as error:
        return error.msg, 404