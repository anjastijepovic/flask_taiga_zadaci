from flask import Flask, request, jsonify
from flask_restful import Api, Resource, reqparse, abort
# from queries import get_employees, get_employee_from_num, add_employee, delete_employee, update_employee, get_customers_with_employee_num, payment_amount, get_all, add_payment, get_payment_from_order_number
# from queries import get_payment_from_customer_num, get_products, delete_product, get_product_from_code, add_product, update_product, get_order_from_number, add_order_and_details, get_customer_from_number, product_has_sufficient_stock
from decimal import Decimal

from queries import *

app = Flask(__name__)
api = Api(app)


employees_args = reqparse.RequestParser()

employees_args.add_argument("employeeNumber", type = int, help="Employee number is required", required = True)
employees_args.add_argument("firstName", type = str, help="First name of the employee is required", required = True)
employees_args.add_argument("lastName", type = str, help="Last name of the employee is required", required = True)
employees_args.add_argument("extension", type = str, help="Employee's extension is required", required = True)
employees_args.add_argument("email", type = str, help="Employee's email is required", required = True)
employees_args.add_argument("officeCode", type = int, help="Office code of the employee is required", required = True)
employees_args.add_argument("reportsTo", type = int, help="ReportsTo number of the employee")
employees_args.add_argument("jobTitle", type = str, help="Employee's job title is required", required = True)


product_args = reqparse.RequestParser()

product_args.add_argument("productCode", type = str, help="Product code is required", required = True)
product_args.add_argument("productName", type = str, help="Name of the product is required", required = True)
product_args.add_argument("productLine", type = str, help="Product line is required", required = True)
product_args.add_argument("productScale", type = str, help="Product scale is required", required = True)
product_args.add_argument("productVendor", type = str, help="Product vendor is required", required = True)
product_args.add_argument("productDescription", type = str, help="Product description is required", required = True)
product_args.add_argument("quantityInStock", type = int, help="Quantity in stock is required", required = True)
product_args.add_argument("buyPrice", type = float, help="Buy price is required", required = True)
product_args.add_argument("MSRP", type = float, help="MSRP is required", required = True)


orders_args = reqparse.RequestParser()

orders_args.add_argument("orderNumber", type = int, help="Order number is required", required = True)
orders_args.add_argument("orderDate", type = str, help="Order date is required", required = True)
orders_args.add_argument("requiredDate", type = str, help="Required dateis required", required = True)
orders_args.add_argument("shippedDate", type = str, help="Shipped date is required", required = False)
orders_args.add_argument("status", type = str, help="Status is required", required = True) #dodaj koji sve status moze bit
orders_args.add_argument("comments", type = str, help="Comments are required", required = True)
orders_args.add_argument("customerNumber", type = int, help="Customer number is required", required = True)
orders_args.add_argument("productCode", type = str, help="Product code is required", required = True)
orders_args.add_argument("quantityOrdered", type = int, help="Quantity ordered is required", required = True)
orders_args.add_argument("priceEach", type = float, help="Price for each product is required", required = True) 
orders_args.add_argument("orderLineNumber", type = int, help="Order line number is required", required = True)


payment_args = reqparse.RequestParser()

payment_args.add_argument("orderNumber", type = int, help="Order number is required", required = True)
payment_args.add_argument("checkNumber", type = str, help="Check numberis required", required = True)

# Funkcija za serijalizaciju Decimal objekata
def decimal_serializer(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    

#EMPLOYEES - ABORT

def abort_if_employee_doesnt_exist(employee_number):
    employee = get_employee_from_num(employee_number)
    if employee is None:
        abort(404, message="Employee with specified employee number does not exist.")


# def abort_if_employee_exists(employee_number):
#     employee = get_employee_from_num(employee_number)
#     if employee is not None:
#         abort(409, message="Employee with specified employee number already exist.")

        
# #CUSTOMERS - ABORT
        
# def abort_if_sales_rep_doesnt_exist(sales_emp_num):
#     customers = get_customers_with_employee_num(sales_emp_num)
#     if len(customers) == 0:
#         abort(404, message="Customers with specified sales rep employee number do not exist.")

# #PAYMENTS - ABORT
        
# def abort_if_payment_doesnt_exist(customer_number):
#     payment = get_payment_from_customer_num(customer_number)
#     if payment is None:
#         abort(404, message="Payment from specified customer number does not exist.")


def abort_if_payment_exists(order_number):
    payment = get_payment_from_order_number(order_number)
    if payment is not None:
        abort(409, message="Payment of specified order already exists.")

# #PRODUCTS - ABORT
        
def abort_if_product_doesnt_exist(product_code):
    product = get_product_from_code(product_code)
    if product is None:
        abort(404, message="Product with specified product code does not exist.")

# def abort_if_product_exists(product_code):
#     product = get_product_from_code(product_code)
#     if product is not None:
#         abort(409, message="Product with specified product code already exist.")


# # ORDERS i order details ABORT
        
# #abort if order already exists
        
def abort_if_order_exists(order_number):
    order = get_order_from_number(order_number)
    if order is not None:
        abort(409, message="Order with specified number already exist.")

# #abort if customer doesnt exist

def abort_if_customer_doesnt_exist(customer_number):
    customer = get_customer_from_number(customer_number)
    if customer is None:
        abort(404, message="Customer with specified number does not exist.")

# #abort if quantity in stock is not sufficient
        
def abort_if_quantity_not_sufficient(product_code, quantity_ordered):
        if not product_has_sufficient_stock(product_code, quantity_ordered):
            abort(404, message="Insufficient stock of the product.")

# #abort_if_order_doesnt_exist
            
def abort_if_order_doesnt_exist(order_number):
    order = get_order_from_number(order_number)
    if order is None:
        abort(404, message="Order with specified number does not exist.")



    #### KLASE

# EMPLOYEES class

class Employees(Resource):

#task 6
    def get(self):
        return get_employees() #status code

    def post(self):
        # employee_number = request.json["employeeNumber"] #cita iz body-ja
        # abort_if_employee_exists(employee_number)
        args = employees_args.parse_args()
        result = add_employee(
        args['employeeNumber'],
        args['lastName'],
        args['firstName'],
        args['extension'],
        args['email'],
        args['officeCode'],
        args['reportsTo'],
        args['jobTitle']
        )
        if result is True:
            return "Successfully added an employee." ,201
        else: 
            return result

        # return {'message': 'Successfully added an employee.'}, 201
        

    def delete(self):
        employee_number = request.args.get('employeeNumber')  # Read from URL query parameter
        abort_if_employee_doesnt_exist('employeeNumber')
        result = delete_employee(employee_number)
        return result
    

    def put(self):
        employee_number = request.json["employeeNumber"]
        abort_if_employee_doesnt_exist(employee_number)
        args = employees_args.parse_args()
        result = update_employee(
        args['employeeNumber'],
        args['lastName'],
        args['firstName'],
        args['extension'],
        args['email'],
        args['officeCode'],
        args['reportsTo'],
        args['jobTitle']
        )
        return result



# CUSTOMERS class
    
class Customers(Resource):
    
#task 7
    def get(self):
        sales_emp_num = request.args.get('salesRepEmployeeNumber')  
        # abort_if_sales_rep_doesnt_exist(sales_emp_num)
        return jsonify(get_customers_with_employee_num(sales_emp_num))
    #vraca listu customers sa istim sales rep employee number (ukoliko salesRepEmployeeNumber iz requesta postoji). dodati ono za decimalne

        

# PAYMENTS class
    
class Payments(Resource):
    
# task 8
    def post(self):
        if 'customerNumber' in request.json and 'paymentDate' in request.json:
            customer_number = request.json['customerNumber']
            #abort_if_payment_doesnt_exist(customer_number)
            payment_date = request.json["paymentDate"]
            return payment_amount(customer_number, payment_date)
        elif 'orderNumber' in request.json and 'checkNumber' in request.json:
            order_number = request.json["orderNumber"]
            abort_if_order_doesnt_exist(order_number)
            abort_if_payment_exists(order_number)
            # jer checkNumber nije unique
            args = payment_args.parse_args()
            result = add_payment(
            args['orderNumber'],
            args['checkNumber'],
            )
            return result
        else:
            return {'error': 'Invalid request body'}, 400


# PRODUCTS class
    
class Products(Resource):

#task 9

    def get(self):
        return get_products()
    
    # def get(self):
    #     products = get_all()
    #     return jsonify(products)


    def delete(self):
        product_code = request.args.get('productCode')  
        abort_if_product_doesnt_exist(product_code)
        result = delete_product(product_code)
        return result
    
    def post(self):
        # product_code = request.json["productCode"]
        # abort_if_product_doesnt_exist(product_code)
        args = product_args.parse_args()
        result = add_product(
        args['productCode'],
        args['productName'],
        args['productLine'],
        args['productScale'],
        args['productVendor'],
        args['productDescription'],
        args['quantityInStock'],
        args['buyPrice'],
        args['MSRP']
        )
        return result
    

    def put(self):
        product_code = request.json["productCode"]
        abort_if_product_doesnt_exist(product_code)
        args = product_args.parse_args()
        result = update_product(
        args['productCode'],
        args['productName'],
        args['productLine'],
        args['productScale'],
        args['productVendor'],
        args['productDescription'],
        args['quantityInStock'],
        args['buyPrice'],
        args['MSRP']
        )
        return result, 201
    
    
    
#novi endpoint
    
@app.route('/products/get_product', methods=['POST'])
def get_product():
    product_code = request.json["productCode"]
    # abort_if_product_doesnt_exist(product_code)
    return jsonify(get_product_from_code(product_code))


# ORDERS class
    
    
class Orders(Resource):

#task 10 
    def post(self):
       
        order_number = request.json["orderNumber"]
        customer_number = request.json["customerNumber"] 
        product_code = request.json["productCode"]
        quantity_ordered = request.json["quantityOrdered"] 

        abort_if_order_exists(order_number)
        abort_if_customer_doesnt_exist(customer_number)
        abort_if_quantity_not_sufficient(product_code, quantity_ordered)

        args = orders_args.parse_args()
        add_order_and_details(
        args['orderNumber'],
        args['orderDate'],
        args['requiredDate'],
        args['shippedDate'],
        args['status'],
        args['comments'],
        args['customerNumber'],
        args['productCode'],
        args['quantityOrdered'],
        args['priceEach'],
        args['orderLineNumber']
        )
        return {'message': 'Successfully added an order and its details.'}, 201

    
api.add_resource(Employees, "/employees") 
api.add_resource(Customers, "/employees/customers")
api.add_resource(Payments, "/customers/payments") 

api.add_resource(Products, "/products") 
api.add_resource(Orders, "/customers/orders") 


if __name__ == "__main__":
    app.run(debug=True)



# Komentari
#Use try and except blocks, then you wont need to check if something exists.
#In try return ok message, in except return error message
#Dont use abort, use return
#In fututre tasks separete everything in different files so it is easier to navigate 
#Payment fja da bude jedna

    
